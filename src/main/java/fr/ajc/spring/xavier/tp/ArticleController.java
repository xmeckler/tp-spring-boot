package fr.ajc.spring.xavier.tp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import model.Article;
import repo.ArticleRepository;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {
	@Autowired
	private ArticleRepository articleRepository;
	
	@GetMapping
	public Iterable<Article> findAll() {
		return articleRepository.findAll();
	}
	
	@GetMapping("/article/{name}")
	public List<Article> findByName(@PathVariable String name) {
		return articleRepository.findByName(name);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Article create(@RequestBody Article article) {
		return articleRepository.save(article);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable long id) {
		articleRepository.deleteById(id);
	}
}
