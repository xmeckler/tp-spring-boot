package fr.ajc.spring.xavier.tp;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import model.Article;
import model.Saison;
import repo.ArticleRepository;

@Controller
public class SimpleController {
	@Value("${spring.application.name}")
	String appName;
	
	@Autowired
	private ArticleRepository articleRepository;
	
	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		return "home";
	}
	
	@GetMapping("/time")
	public String timePage(Model model) {
		model.addAttribute("appName", appName);
		
		LocalDateTime today = LocalDateTime.now();
		model.addAttribute("localDateTime", today);
		
		model.addAttribute("weekend", today.getDayOfWeek() == DayOfWeek.SATURDAY || today.getDayOfWeek() == DayOfWeek.SUNDAY);
		
		Saison winter = new Saison("hiver", false);
		Saison spring = new Saison("printemps", false);
		Saison summer = new Saison("ete", false);
		Saison fall = new Saison("automne", false);
		
		String saison = "";
		if (today.isAfter(LocalDateTime.of(today.getYear(), 12, 20, 0, 0)) || today.isBefore(LocalDateTime.of(today.getYear(), 3, 21, 0, 0))) {
			saison = winter.getName();
			winter.setCurrent(true);
		} else if(today.isAfter(LocalDateTime.of(today.getYear(), 3, 20, 0, 0)) && today.isBefore(LocalDateTime.of(today.getYear(), 6, 21, 0, 0))) {
			saison = spring.getName();
			spring.setCurrent(true);
		} else if(today.isAfter(LocalDateTime.of(today.getYear(), 6, 20, 0, 0)) && today.isBefore(LocalDateTime.of(today.getYear(), 9, 21, 0, 0))) {
			saison = summer.getName();
			summer.setCurrent(true);
		} else {
			saison = fall.getName();
			fall.setCurrent(true);
		}
		model.addAttribute("saison", saison);
		
		List<Saison> seasons = new ArrayList<Saison>();
		seasons.add(spring);
		seasons.add(summer);
		seasons.add(fall);
		seasons.add(winter);
		model.addAttribute("seasons", seasons);
		
		return "time";
	}
	
	@GetMapping("/articles/list")
	public String articlesList(Model model) {
		Iterable<Article> list = articleRepository.findAll();
		model.addAttribute(list);
		return "articlesList";
	}
}
