package fr.ajc.spring.xavier.tp;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import model.Article;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TpApplicationTests {
	
	public static final String API_ROOT = "http://localhost:8081/api/articles";

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void whenGetAllArticles_thenOK() {
		Response response = RestAssured.get(API_ROOT);
		System.out.println(response.body().asString());
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}
	
	private String createArticleAsUri(Article article) {
		Response response =
		RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).body(article).post(API_ROOT);
		return API_ROOT + "/" + response.jsonPath().get("id");
	}
	
	@Test
	public void whenGetArticleByName_thenOK() {
		Article article = createRandomArticle();
		createArticleAsUri(article);
		Response response = RestAssured.get(
		API_ROOT + "/article/" + article.getName());
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
	}
	
	public Article createRandomArticle() {
		Article article = new Article(RandomStringUtils.random(10, true, false), new Random().nextFloat());
		return article;
	}

}
